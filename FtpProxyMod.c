/**
 * Simple FTP Proxy For 2015 Introduction to Computer Network.
 * Author: z58085111 @ HSNL
 * 2015/12
 * **/
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/time.h>
#include	<netinet/tcp.h>		/* for TCP_MAXSEG */

#define MAXSIZE 2048
#define FTP_PORT 8740
#define FTP_PASV_CODE 227
#define FTP_ADDR "140.114.71.159"
#define max(X,Y) ((X) > (Y) ? (X) : (Y))



/*---------------------------------------------------------*/
//variable declaration
#define SEGMENTS 1
struct timeval ini_ptos, fin_ptos;
struct timeval ini_ptoc, fin_ptoc;
double timefrag= 1000.0/SEGMENTS;       //ms
int downTotal = 0, upTotal = 0;

int sndbuf = 1024;
int rcvbuf = 1024;
int tcp_maxseg = 256;
socklen_t tcp_maxseg_len = sizeof(tcp_maxseg);
/*---------------------------------------------------------*/



int proxy_IP[4];

int connect_FTP(int ser_port, int clifd);
int proxy_func(int ser_port, int clifd, int rate);
int create_server(int port);
void rate_control(int byte, int *byteTotal, int rate, struct timeval *start, struct timeval *end);

int main (int argc, char **argv) {
    int ctrlfd, connfd, port, rate = 0;
    pid_t childpid;
    socklen_t clilen;
    struct sockaddr_in cliaddr;
    
    
    
    
    
    if (argc < 3) {
        printf("[v] Usage: ./executableFile <ProxyIP> <ProxyPort> \n");
        return -1;
    }
    
    sscanf(argv[1], " %d.%d.%d.%d", &proxy_IP[0], &proxy_IP[1], &proxy_IP[2], &proxy_IP[3]);
    port = atoi(argv[2]);
    
    rate = atoi(argv[3]);
    
    ctrlfd = create_server(port);
    clilen = sizeof(struct sockaddr_in);
    //control port listening
    for (;;) {
        connfd = accept(ctrlfd, (struct sockaddr *)&cliaddr, &clilen);
        if (connfd < 0) {
            printf("[x] Accept failed\n");
            return 0;
        }
        
        printf("[v] Client: %s:%d connect!\n", inet_ntoa(cliaddr.sin_addr), htons(cliaddr.sin_port));
        //client connection handling
        if ((childpid = fork()) == 0) {
            close(ctrlfd);
            proxy_func(FTP_PORT, connfd, rate);
            printf("[v] Client: %s:%d terminated!\n", inet_ntoa(cliaddr.sin_addr), htons(cliaddr.sin_port));
            exit(0);
        }
        
        close(connfd);
    }
    return 0;
}

int connect_FTP(int ser_port, int clifd) {
    int sockfd;
    char addr[] = FTP_ADDR;
    int byte_num;
    char buffer[MAXSIZE];
    struct sockaddr_in servaddr;
    
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("[x] Create socket error");
        return -1;
    }
    
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(ser_port);
    
    
    
    
   
    if (inet_pton(AF_INET, addr, &servaddr.sin_addr) <= 0) {
        printf("[v] Inet_pton error for %s", addr);
        return -1;
    }
    /*----------------------------------------------------------*/

    //setsockopt(sockfd, IPPROTO_TCP, TCP_MAXSEG, &tcp_maxseg, tcp_maxseg_len);
    //setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &rcvbuf, sizeof(rcvbuf));
    //setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &sndbuf, sizeof(sndbuf));
     /*----------------------------------------------------------*/
    if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
        printf("[x] Connect error");
        return -1;
    }
    
    printf("[v] Connect to FTP server\n");
    if (ser_port == FTP_PORT) {
        if ((byte_num = read(sockfd, buffer, MAXSIZE)) <= 0) {
            printf("[x] Connection establish failed.\n");
        }
        /*------------------------------------------*/
        gettimeofday(&ini_ptos,NULL);

        
        /*-------------------------------------------*/
        if (write(clifd, buffer, byte_num) < 0) {
            printf("[x] Write to client failed.\n");
            return -1;
        }
    }
    
    return sockfd;
}

int proxy_func(int ser_port, int clifd, int rate) {
    /*------------------------------------------*/
    gettimeofday(&ini_ptoc,NULL);
    /*-------------------------------------------*/
    
    char buffer[MAXSIZE];
    int serfd = -1, datafd = -1, connfd;
    int data_port;
    int byte_num;
    int status, pasv[7];
    int childpid;
    socklen_t clilen;
    struct sockaddr_in cliaddr;
    
    // select vars
    int maxfdp1;
    int i, nready = 0;
    fd_set rset, allset;
    
    // connect to FTP server
    if ((serfd =  connect_FTP(ser_port, clifd)) < 0) {
        printf("[x] Connect to FTP server failed.\n");
        return -1;
    }
    
    datafd = serfd;
    
    // initialize select vars
    FD_ZERO(&allset);
    FD_SET(clifd, &allset);
    FD_SET(serfd, &allset);
    
    // selecting
    for (;;) {
        // reset select vars
        rset = allset;
        maxfdp1 = max(clifd, serfd) + 1;
        
        // select descriptor
        nready = select(maxfdp1 + 1, &rset, NULL, NULL, NULL);
        if (nready > 0) {
            
            
            // check FTP client socket fd
            if (FD_ISSET(clifd, &rset)) {
                /*----------------------------------------------------*/
                gettimeofday(&fin_ptoc, NULL);
                /*----------------------------------------------------*/

                memset(buffer, 0, MAXSIZE);
                if ((byte_num = read(clifd, buffer, MAXSIZE)) <= 0) {
                    printf("[!] Client terminated the connection.\n");
                    break;
                }
                /*----------------------------------------------------*/
                printf("upload\n");
                rate_control(byte_num, &upTotal, rate, &ini_ptoc, &fin_ptoc);
                
                
                
                
                /*----------------------------------------------------*/
                
                if (write(serfd, buffer, byte_num) < 0) {
                    printf("[x] Write to server failed.\n");
                    break;
                }
                
            }
            
            
            // check FTP server socket fd
            if (FD_ISSET(serfd, &rset)) {
                /*----------------------------------------------------*/
                gettimeofday(&fin_ptos, NULL);
                /*----------------------------------------------------*/
                memset(buffer, 0, MAXSIZE);
                if ((byte_num = read(serfd, buffer, MAXSIZE)) <= 0) {
                    printf("[!] Server terminated the connection.\n");
                    break;
                }
            
                if(ser_port == FTP_PORT)
                buffer[byte_num] = '\0';
            
                status = atoi(buffer);
            
                if (status == FTP_PASV_CODE && ser_port == FTP_PORT) {
                
                    sscanf(buffer, "%d Entering Passive Mode (%d,%d,%d,%d,%d,%d)",&pasv[0],&pasv[1],&pasv[2],&pasv[3],&pasv[4],&pasv[5],&pasv[6]);
                    memset(buffer, 0, MAXSIZE);
                    sprintf(buffer, "%d Entering Passive Mode (%d,%d,%d,%d,%d,%d)\n", status, proxy_IP[0], proxy_IP[1], proxy_IP[2], proxy_IP[3], pasv[5], pasv[6]);
                
                    if ((childpid = fork()) == 0) {
                        data_port = pasv[5] * 256 + pasv[6];
                        datafd = create_server(data_port);
                        printf("[-] Waiting for data connection!\n");
                        clilen = sizeof(struct sockaddr_in);
                        connfd = accept(datafd, (struct sockaddr *)&cliaddr, &clilen);
                        if (connfd < 0) {
                            printf("[x] Accept failed\n");
                            return 0;
                        }
                    
                        printf("[v] Data connection from: %s:%d connect.\n", inet_ntoa(cliaddr.sin_addr), htons(cliaddr.sin_port));
                        proxy_func(data_port, connfd, rate);
                        printf("[!] End of data connection!\n");
                        exit(0);
                    }
                }
                /*----------------------------------------------------*/
                printf("download\n");
                rate_control(byte_num, &downTotal, rate, &ini_ptos, &fin_ptos);
                
                
            
                /*----------------------------------------------------*/
                if (write(clifd, buffer, byte_num) < 0) {
                    printf("[x] Write to client failed.\n");
                    break;
                }
            }
            
            
            
        } else {
            printf("[x] Select() returns -1. ERROR!\n");
            return -1;
        }
    }
    return 0;
}

int create_server(int port) {
    int listenfd;
    struct sockaddr_in servaddr;
    
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);
    /*----------------------------------------*/
    
    //setsockopt(listenfd, IPPROTO_TCP, TCP_MAXSEG, &tcp_maxseg, tcp_maxseg_len);
    //setsockopt(listenfd, SOL_SOCKET, SO_RCVBUF, &rcvbuf, sizeof(rcvbuf));
    //setsockopt(listenfd, SOL_SOCKET, SO_SNDBUF, &sndbuf, sizeof(sndbuf));
    /*----------------------------------------*/
    
    if (bind(listenfd, (struct sockaddr *)&servaddr , sizeof(servaddr)) < 0) {
        //print the error message
        perror("bind failed. Error");
        return -1;
    }
    
    listen(listenfd, 3);
    return listenfd;
}


void rate_control(int byte, int *byteTotal, int rate, struct timeval *start, struct timeval *end)
{
    double byteSeg = (double)rate * timefrag;       //(KB/s) * ms = Byte
    double sec =  (double)(end->tv_sec  - start->tv_sec) * 1000.0;
    double usec = (double)(end->tv_usec - start->tv_usec) / 1000.0;
    double elapsed  = sec + usec;    //ms
    
    if(elapsed < timefrag)
    {
        if(*byteTotal + byte > byteSeg){
            usleep((timefrag - elapsed) * 1000);
            *byteTotal = 0;
        }
        else{
            *byteTotal += byte;
        }
    }
    else if(elapsed == timefrag)
    {
        *start = *end;
        *byteTotal = 0;
    }
    else{
        start->tv_sec +=  (int)timefrag / 1000;
        start->tv_usec += ((int)timefrag%1000) * 1000;
    }
    printf("%lf(ms), %d, %d\n", elapsed, byte, *byteTotal);
}
